var express = require('express')
var onoff = require('onoff')

const app = express()
const port = 3000

let status = false;
const led = new onoff.Gpio(17, 'out')

app.get('/',(req,res) => {
    res.json({title:'index'})
})

app.get('/toggle',(req, res) => {
    status = !status
    led.writeSync(status ? 1: 0);
    res.json({status: status})
})

process.on('SIGINT', ()=>{led.unexport()})


app.listen(port, ()=>{console.log(`App running on ${port}`)})